using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using MovieCharactersAPI;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Services;
using System.Reflection;

namespace MovieCharactersAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Adds services to the container.
            builder.Services.AddControllers();

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            // Adds connection to the database.
            builder.Services.AddDbContext<MoviesDbContext>(Option=>Option.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

            // Add API documentation
            builder.Services.AddSwaggerGen(o =>
            {
                o.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "MovieCharacters API",
                    Version = "v1",
                    Description = "An ASP.NET Core Web API to track movies and their characters and franchises.",
                });
                // Set the comments path for the Swagger JSON and UI
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                o.IncludeXmlComments(xmlPath);
            });

            // Adds automapper to the build.
            builder.Services.AddAutoMapper(typeof(Program));

            // Adds Service classes to the build.
            builder.Services.AddScoped(typeof(ICharacterService), typeof(CharacterService));
            builder.Services.AddScoped(typeof(IMovieService), typeof(MovieService));
            builder.Services.AddScoped(typeof(IFranchiseService), typeof(FranchiseService));

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}