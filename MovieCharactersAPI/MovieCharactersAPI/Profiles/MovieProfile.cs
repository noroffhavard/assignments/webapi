﻿using AutoMapper;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DTOs.Movie;

namespace MovieCharactersAPI.Profiles
{
    /// <summary>
    /// Map class for mapping Movie with DTOs
    /// </summary>
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>();
            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieEditDTO, Movie>();
        }
    }
}