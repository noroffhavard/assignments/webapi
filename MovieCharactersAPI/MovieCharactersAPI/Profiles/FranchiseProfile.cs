﻿using AutoMapper;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DTOs.Franchise;

namespace MovieCharactersAPI.Profiles
{
    /// <summary>
    /// Map class for mapping Franchise with DTOs
    /// </summary>
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>();
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();
        }
    }
}