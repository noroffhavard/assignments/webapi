﻿using AutoMapper;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DTOs.Character;

namespace MovieCharactersAPI.Profiles
{
    /// <summary>
    /// Map class for mapping Character with DTOs
    /// </summary>
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>();
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
