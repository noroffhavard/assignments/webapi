﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DTOs.Character;
using MovieCharactersAPI.Model.DTOs.Movie;
using MovieCharactersAPI.Services;

namespace MovieCharactersAPI.Controllers
{
    /// <summary>
    /// Controller class for movie
    /// </summary>
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Get all movies.
        /// </summary>
        /// <returns> All movies </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }

        /// <summary>
        /// Get a specific movie by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> A specific movie </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieService.GetSpecificMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Edit a movie.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            //Map to domaine
            Movie domaineMovie = _mapper.Map<Movie>(movie);
            await _movieService.UpdateMovieAsync(domaineMovie);

            return NoContent();
        }

        /// <summary>
        /// Add a new movie.
        /// </summary>
        /// <param name="movieDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO movieDto)
        {
            var domaineMovie = _mapper.Map<Movie>(movieDto);
            domaineMovie = await _movieService.AddMovieAsync(domaineMovie);

            return CreatedAtAction("GetMovie", new { id = domaineMovie.Id }, _mapper.Map<MovieReadDTO>(domaineMovie));
        }

        /// <summary>
        /// Delete a specific movie.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Edit what characters a movie contains.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns></returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateMovieCharactersAsync(id, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid character");
            }

            return NoContent();
        }
        
        /// <summary>
        /// Get all characters in a specific movie.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> Characters in movie </returns>
        [HttpGet("{id}/movieCharacters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetMovieCharacters(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            var domaineCharacters = await _movieService.GetMovieCharactersAsync(id);

            return _mapper.Map<List<CharacterReadDTO>>(domaineCharacters);
        }
    }
}