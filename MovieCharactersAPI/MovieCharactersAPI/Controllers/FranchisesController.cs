﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DTOs.Character;
using MovieCharactersAPI.Model.DTOs.Franchise;
using MovieCharactersAPI.Model.DTOs.Movie;
using MovieCharactersAPI.Services;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Get all franchises.
        /// </summary>
        /// <returns> All franchises </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Get a specific franchise by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> A specific franchise </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var domaineFranchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            if (domaineFranchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(domaineFranchise);
        }

        /// <summary>
        /// Edit a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchiseDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchiseDto)
        {
            if (id != franchiseDto.Id)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            //Map to domaine
            Franchise domaineFranchise = _mapper.Map<Franchise>(franchiseDto);
            await _franchiseService.UpdateFranchiseAsync(domaineFranchise);

            return NoContent();
        }

        /// <summary>
        /// Add a new franchise.
        /// </summary>
        /// <param name="franchiseDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchiseDto)
        {
            var domaineFranchise = _mapper.Map<Franchise>(franchiseDto);
            domaineFranchise = await _franchiseService.AddFranchiseAsync(domaineFranchise);

            return CreatedAtAction("GetFranchise", new { id = domaineFranchise.Id }, _mapper.Map<FranchiseReadDTO>(domaineFranchise));
        }

        /// <summary>
        /// Delete a specific franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }
        
        /// <summary>
        /// Edit a what movies a franchise contains.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            if(!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateFranchiseMoviesAsync(id, movies);
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest("Invalid movie");
            }
            
            return NoContent();
        }

        /// <summary>
        /// Get all movies in a specific franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> Movies in a specific franchise </returns>
        [HttpGet("{id}/franchiseMovies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetFranchiseMovies(int id)
        {
            if(!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            var domaineMovies = await _franchiseService.GetFranchiseMoviesAsync(id);

            return _mapper.Map<List<MovieReadDTO>>(domaineMovies);
        }

        /// <summary>
        /// Get all characters in a specific franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> Characters in a specific franchise </returns>
        [HttpGet("{id}/franchiseCharacters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetFranchiseCharactersAsync(int id)
        {
            if(!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            var domaineCharacters = await _franchiseService.GetFranchiseCharactersAsync(id);

            return _mapper.Map<List<CharacterReadDTO>>(domaineCharacters);
        }
    }
}