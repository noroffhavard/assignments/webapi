﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DTOs.Character;
using MovieCharactersAPI.Services;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharactersController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Get all characters.
        /// </summary>
        /// <returns> All characters </returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());
        }

        /// <summary>
        /// Get a specific character by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns> A specific character </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var domaineCharacter = await _characterService.GetSpecificCharacterAsync(id);

            if (domaineCharacter == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(domaineCharacter);
        }

        /// <summary>
        /// Edit a specific character.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDto)
        {
            if (id != characterDto.Id)
            {
                return BadRequest();
            }

            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            //Map to domaine
            Character domaineCharacter = _mapper.Map<Character>(characterDto);
            await _characterService.UpdateCharacterAsync(domaineCharacter);

            return NoContent();
        }

        /// <summary>
        /// Add a new character.
        /// </summary>
        /// <param name="characterDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO characterDto)
        {
            var domaineCharacter = _mapper.Map<Character>(characterDto);
            domaineCharacter = await _characterService.AddCharacterAsync(domaineCharacter);

            return CreatedAtAction("GetCharacter", new { id = domaineCharacter.Id }, _mapper.Map<CharacterReadDTO>(domaineCharacter));
        }

        /// <summary>
        /// Delete a specific character.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterService.DeleteCharacterAsync(id);

            return NoContent();
        }
    }
}