﻿using MovieCharactersAPI.Model;

namespace MovieCharactersAPI.Services
{
    public interface IMovieService
    {
        /// <summary>
        /// Checks if a movie by id exists in the DB
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        public bool MovieExists(int movieId);

        /// <summary>
        /// Get all movies in the DB.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();

        /// <summary>
        /// Get a specific movie by id.
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        public Task<Movie> GetSpecificMovieAsync(int movieId);

        /// <summary>
        /// Add a new movie to the DB.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task<Movie> AddMovieAsync(Movie movie);

        /// <summary>
        /// Update a movie in the DB.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task UpdateMovieAsync(Movie movie);

        /// <summary>
        /// Update what characters are in what movies.
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characters"></param>
        /// <returns></returns>
        public Task UpdateMovieCharactersAsync(int movieId, List<int> characters);

        /// <summary>
        /// Delete a specific movie.
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        public Task DeleteMovieAsync(int movieId);

        /// <summary>
        /// Get all characters in a specific movie.
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetMovieCharactersAsync(int movieId);
    }
}