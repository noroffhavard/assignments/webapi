﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DTOs.Character;
using MovieCharactersAPI.Model.DTOs.Movie;

namespace MovieCharactersAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly MoviesDbContext _context;

        public MovieService(MoviesDbContext context)
        {
            _context = context;
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        public async Task DeleteMovieAsync(int movieId)
        {
            var movie = await _context.Movies.FindAsync(movieId);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetMovieCharactersAsync(int movieId)
        {
            var domaineMovie = await _context.Movies.FindAsync(movieId);

            var domaineCharacters = await _context.Characters
                .Where(x => x.Movies.Contains(domaineMovie))
                .ToListAsync();

            return domaineCharacters;
        }

        public async Task<Movie> GetSpecificMovieAsync(int movieId)
        {
            return await _context.Movies.FindAsync(movieId);
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(m => m.Id == id);
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMovieCharactersAsync(int movieId, List<int> characters)
        {
            Movie movieToUpdateChars = await _context.Movies
                .Include(x => x.Characters)
                .Where(x => x.Id == movieId)
                .FirstAsync();

            List<Character> chars = new List<Character>();
            foreach (int charId in characters)
            {
                Character charr = await _context.Characters.FindAsync(charId);
                if(charr == null)
                {
                    throw new KeyNotFoundException();
                }
                chars.Add(charr);
            }
            movieToUpdateChars.Characters = chars;
            await _context.SaveChangesAsync();
        }
    }
}