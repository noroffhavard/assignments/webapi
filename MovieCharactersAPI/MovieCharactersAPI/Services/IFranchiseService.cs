﻿using Microsoft.AspNetCore.Mvc;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DTOs.Franchise;

namespace MovieCharactersAPI.Services
{
    public interface IFranchiseService
    {
        /// <summary>
        /// Checks if franchise by id exists in the DB.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public bool FranchiseExists(int franchiseId);

        /// <summary>
        /// Get all franchises in the DB.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();

        /// <summary>
        /// Get a specific franchise by id.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public Task<Franchise> GetSpecificFranchiseAsync(int franchiseId);

        /// <summary>
        /// Add a new franchise to the DB.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Update a franchise in the DB.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public Task UpdateFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Update what movies a franchise contains.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        public Task UpdateFranchiseMoviesAsync(int franchiseId, List<int> movies);

        /// <summary>
        /// Delete a specific franchise.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public Task DeleteFranchiseAsync(int franchiseId);

        /// <summary>
        /// Get all movies in a specific franchise.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetFranchiseMoviesAsync(int franchiseId);

        /// <summary>
        /// Get all characters in a specific franchise.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetFranchiseCharactersAsync(int franchiseId);
    }
}