﻿using MovieCharactersAPI.Model;

namespace MovieCharactersAPI.Services
{
    public interface ICharacterService
    {
        /// <summary>
        /// Checks if the character by id exists in the DB.
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public bool CharacterExists(int characterId);

        /// <summary>
        /// Get all characters in the DB.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync();

        /// <summary>
        /// Get a specific character by id.
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns></returns>
        public Task<Character> GetSpecificCharacterAsync(int movieId);

        /// <summary>
        /// Add a new character to the DB.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task<Character> AddCharacterAsync(Character character);

        /// <summary>
        /// Update a specific character.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task UpdateCharacterAsync(Character character);

        /// <summary>
        /// Delete a character in the DB.
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public Task DeleteCharacterAsync(int characterId);
    }
}
