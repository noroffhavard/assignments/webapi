﻿
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Model;
using MovieCharactersAPI.Model.DTOs.Character;
using MovieCharactersAPI.Model.DTOs.Franchise;
using MovieCharactersAPI.Model.DTOs.Movie;

namespace MovieCharactersAPI.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MoviesDbContext _context;

        public FranchiseService(MoviesDbContext context)
        {
            _context = context;
        }

        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int franchiseId)
        {
            var franchise = await _context.Franchises.FindAsync(franchiseId);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int franchiseId)
        {
            return _context.Franchises.Any(f => f.Id == franchiseId);
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetFranchiseCharactersAsync(int franchiseId)
        {
            var movie = await _context.Movies.FindAsync(franchiseId);

            var domaineCharacters = await _context.Movies
                .Where(x => x.FranchiseId == franchiseId)
                .SelectMany(x => x.Characters)
                .GroupBy(c => c.Id).Select(c => c.First())
                .ToListAsync();

            return domaineCharacters;
        }

        public async Task<IEnumerable<Movie>> GetFranchiseMoviesAsync(int movieId)
        {
            var domaineFranchise = await _context.Franchises.FindAsync(movieId);

            var domaineMovies = await _context.Movies
                .Where(x => x.FranchiseId == movieId)
                .ToListAsync();
            
            return domaineMovies;
        }

        public async Task<Franchise> GetSpecificFranchiseAsync(int franchiseId)
        {
            return await _context.Franchises.FindAsync(franchiseId);
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateFranchiseMoviesAsync(int franchiseId, List<int> movies)
        {
            Franchise franchiseToUpdateMovies = await _context.Franchises
                .Include(x => x.Movies)
                .Where(x => x.Id == franchiseId)
                .FirstAsync();

            List<Movie> chars = new List<Movie>();
            foreach (int movId in movies)
            {
                Movie mov = await _context.Movies.FindAsync(movId);
                if (mov == null)
                {
                    throw new KeyNotFoundException();
                }
                chars.Add(mov);
            }
            franchiseToUpdateMovies.Movies = chars;
            await _context.SaveChangesAsync();
        }
    }
}
