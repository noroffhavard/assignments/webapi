﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieCharactersAPI.Migrations
{
    public partial class initialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "varchar(40)", nullable: false),
                    Alias = table.Column<string>(type: "varchar(40)", nullable: false),
                    Gender = table.Column<string>(type: "varchar(20)", nullable: false),
                    Picture = table.Column<string>(type: "varchar(MAX)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(40)", nullable: false),
                    Description = table.Column<string>(type: "varchar(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "varchar(40)", nullable: false),
                    Genre = table.Column<string>(type: "varchar(40)", nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "varchar(40)", nullable: false),
                    Picture = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    Trailer = table.Column<string>(type: "varchar(MAX)", nullable: false),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false),
                    MovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.CharacterId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Jedi", "Luke Skywalker", "Male", "https://d.wattpad.com/story_parts/673752130/images/1574ff177d6e82e1726895238013.jpg" },
                    { 2, "Sith Lord", "Darth Vader", "Male", "https://th.bing.com/th/id/R.4fc29fd5431a5e58bdd088254ea0d63f?rik=0GIIU8tiHFETng&pid=ImgRaw&r=0" },
                    { 3, "Yes", "Princess Leia", "Female", "https://tse3.mm.bing.net/th/id/OIP.jWzu18XPy0MTu8UxuMyhjQHaFe?pid=ImgDet&rs=1" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "All of the star wars movies including spin-off series", "Star wars" },
                    { 2, "Very nice", "Hollywood" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { 1, "George Lucas", 1, "Scfi-Fi, Adventure, Action", "https://th.bing.com/th/id/R.957d5cb578716e6de70fb2e08e704617?rik=Gcd5Tj2UfHyVBw&riu=http%3a%2f%2fwww.new-video.de%2fco%2fstarwars-epi4.jpg&ehk=ghQODivPT3qDhYUGzpvzy8SPihsBqp1%2bvgQzRVYKDhU%3d&risl=&pid=ImgRaw&r=0", 1977, "Star Wars 4", "https://www.youtube.com/watch?v=MpkrMqmmy5k" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { 2, "Steven Spielberg", 2, "Action, War", "https://th.bing.com/th/id/R.7ab4c3d1d606f45f29cfce051c816dbf?rik=gOmrmxLlVeJopg&pid=ImgRaw&r=0", 2001, "Saving Private Ryan", "https://www.youtube.com/watch?v=zwhP5b4tD6g" });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[] { 2, 2 });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MovieId",
                table: "CharacterMovie",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
