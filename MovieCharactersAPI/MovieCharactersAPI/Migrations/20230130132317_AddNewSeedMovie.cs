﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieCharactersAPI.Migrations
{
    public partial class AddNewSeedMovie : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { 3, "Bjørn Bjørnson", 2, "Action, Adventure", "https://th.bing.com/th/id/R.7ab4c3d1d3606f45f29cfce051c816dbf?rik=gOmrmxLlVeJopg&pid=ImgRaw&r=0", 2023, "Monkeyman", "https://www.youtube.com/watch?v=zwfhP5b4tD6g" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}