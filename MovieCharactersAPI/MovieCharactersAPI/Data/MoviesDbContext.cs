﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Model;

namespace MovieCharactersAPI.Data
{
    public class MoviesDbContext : DbContext
    {
        // Database tables.
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MoviesDbContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Seeds data to the database, included data to the many to many linking table MovieCharacters using modelBuilder.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(SeedDataHelper.GetMovies());
            modelBuilder.Entity<Character>().HasData(SeedDataHelper.GetCharacters());
            modelBuilder.Entity<Franchise>().HasData(SeedDataHelper.GetFranchises());

            // Seed many to many Character-Movie relationship. Defines m2m relationship and access linking table.
            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                "CharacterMovie",
                r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                je =>
                {
                    je.HasKey("CharacterId", "MovieId");
                    je.HasData(
                        new { CharacterId = 1, MovieId = 1 },
                        new { CharacterId = 2, MovieId = 2 },
                        new { CharacterId = 3, MovieId = 1 },
                        new { CharacterId = 1, MovieId = 2 }
                        );
                });
        }
        
    }
}