﻿using MovieCharactersAPI.Model;

namespace MovieCharactersAPI.Data
{
    /// <summary>
    /// Seeds data to the three tables in the DB, Characters, Movies and Franchises.
    /// </summary>
    public class SeedDataHelper
    {
        /// <summary>
        /// Seeds data to the character table.
        /// </summary>
        /// <returns></returns>
        public static List<Character> GetCharacters()
        {
            List<Character> characters = new List<Character>()
            {
                new Character()
                {
                    Id = 1,
                    FullName = "Luke Skywalker",
                    Alias = "Jedi",
                    Gender = "Male",
                    Picture = "https://d.wattpad.com/story_parts/673752130/images/1574ff177d6e82e1726895238013.jpg",
                },

                new Character()
                { 
                    Id = 2,
                    FullName = "Darth Vader", 
                    Alias = "Sith Lord", 
                    Gender = "Male", 
                    Picture = "https://th.bing.com/th/id/R.4fc29fd5431a5e58bdd088254ea0d63f?rik=0GIIU8tiHFETng&pid=ImgRaw&r=0"
                },

                new Character() 
                { 
                    Id = 3, 
                    FullName = "Princess Leia",
                    Alias = "Yes", 
                    Gender = "Female",
                    Picture = "https://tse3.mm.bing.net/th/id/OIP.jWzu18XPy0MTu8UxuMyhjQHaFe?pid=ImgDet&rs=1"
                },
            };
            return characters;
        }

        /// <summary>
        /// Seeds data to the movie table.
        /// </summary>
        /// <returns></returns>
        public static List<Movie> GetMovies()
        {
            List<Movie> movies = new List<Movie>()
            {
                new Movie()
                {
                    Id = 1,
                    Title = "Star Wars 4",
                    Genre = "Scfi-Fi, Adventure, Action",
                    ReleaseYear = 1977,
                    Director = "George Lucas",
                    Picture = "https://th.bing.com/th/id/R.957d5cb578716e6de70fb2e08e704617?rik=Gcd5Tj2UfHyVBw&riu=http%3a%2f%2fwww.new-video.de%2fco%2fstarwars-epi4.jpg&ehk=ghQODivPT3qDhYUGzpvzy8SPihsBqp1%2bvgQzRVYKDhU%3d&risl=&pid=ImgRaw&r=0",
                    Trailer = "https://www.youtube.com/watch?v=MpkrMqmmy5k",
                    FranchiseId = 1
                },

                new Movie()
                {
                    Id = 2,
                    Title = "Saving Private Ryan",
                    Genre = "Action, War",
                    ReleaseYear = 2001,
                    Director = "Steven Spielberg",
                    Picture = "https://th.bing.com/th/id/R.7ab4c3d1d606f45f29cfce051c816dbf?rik=gOmrmxLlVeJopg&pid=ImgRaw&r=0",
                    Trailer = "https://www.youtube.com/watch?v=zwhP5b4tD6g",
                    FranchiseId = 2
                },
                new Movie()
                {
                    Id = 3,
                    Title = "Monkeyman",
                    Genre = "Action, Adventure",
                    ReleaseYear = 2023,
                    Director = "Bjørn Bjørnson",
                    Picture = "https://th.bing.com/th/id/R.7ab4c3d1d3606f45f29cfce051c816dbf?rik=gOmrmxLlVeJopg&pid=ImgRaw&r=0",
                    Trailer = "https://www.youtube.com/watch?v=zwfhP5b4tD6g",
                    FranchiseId = 2
                }
            };
            return movies;
        }
    
        /// <summary>
        /// Seed data to the franchise table.
        /// </summary>
        /// <returns></returns>
        public static List<Franchise> GetFranchises()
        {
            List<Franchise> franchises = new List<Franchise>()
            {
                new Franchise()
                {
                    Id = 1,
                    Name = "Star wars",
                    Description = "All of the star wars movies including spin-off series"
                },
                new Franchise()
                {
                    Id = 2,
                    Name = "Hollywood",
                    Description = "Very nice"
                },
            };
            return franchises;
        }
    }
}