﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Model
{
    public class Movie
    {
        // Movie table columns.
        [Key]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(40)")]
        public string Title { get; set; }
        [Required]
        [Column(TypeName = "varchar(40)")]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [Required]
        [Column(TypeName = "varchar(40)")]
        public string Director { get; set; }
        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string Picture { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Trailer { get; set; }
        public ICollection<Character>? Characters { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
    }
}