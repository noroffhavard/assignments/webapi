﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Model
{
    public class Character
    {
        // Character Table columns.
        [Key]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(40)")]
        public string FullName { get; set; }
        [Column(TypeName = "varchar(40)")]
        public string Alias { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string Gender { get; set; }
        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string Picture { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}