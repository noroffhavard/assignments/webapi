﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Model
{
    public class Franchise
    {
        // Franchise table columns.
        [Key]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(40)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string Description { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}