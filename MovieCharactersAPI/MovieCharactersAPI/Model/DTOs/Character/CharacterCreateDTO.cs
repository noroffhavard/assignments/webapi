﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Model.DTOs.Character
{
    /// <summary>
    /// Data Transfer Objects for creating Characters.
    /// </summary>
    public class CharacterCreateDTO
    {
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
    }
}