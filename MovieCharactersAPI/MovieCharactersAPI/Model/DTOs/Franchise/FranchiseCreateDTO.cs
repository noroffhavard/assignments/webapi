﻿namespace MovieCharactersAPI.Model.DTOs.Franchise
{
    /// <summary>
    /// Data Tansfer Object for creaint Franchises.
    /// </summary>
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}