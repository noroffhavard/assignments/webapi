﻿namespace MovieCharactersAPI.Model.DTOs.Franchise
{
    /// <summary>
    /// Data Tansfer Object for editing Franchises.
    /// </summary>
    public class FranchiseEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}